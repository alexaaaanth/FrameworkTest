import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.perfmon
import jetbrains.buildServer.configs.kotlin.buildSteps.maven
import jetbrains.buildServer.configs.kotlin.triggers.schedule
import jetbrains.buildServer.configs.kotlin.triggers.vcs
import jetbrains.buildServer.configs.kotlin.vcs.GitVcsRoot

version = "2024.03"

project {

    vcsRoot(HttpsGitlabComAlexaaaanthFrameworkTestGitRefsHeadsMain)

    buildType(Build)
}

object Build : BuildType({
    name = "Build"

    vcs {
        root(HttpsGitlabComAlexaaaanthFrameworkTestGitRefsHeadsMain)
    }

    steps {
        maven {
            id = "Maven2"
            goals = "clean test"
            runnerArgs = "-Dmaven.test.failure.ignore=true"
        }
    }

    triggers {
        vcs {
        }
        schedule {
            branchFilter = """
                +:*
                +:<default>
            """.trimIndent()
            triggerBuild = always()
        }
    }

    features {
        perfmon {
        }
    }
})

object HttpsGitlabComAlexaaaanthFrameworkTestGitRefsHeadsMain : GitVcsRoot({
    name = "https://gitlab.com/alexaaaanth/FrameworkTest.git#refs/heads/main"
    url = "https://gitlab.com/alexaaaanth/FrameworkTest.git"
    branch = "refs/heads/main"
    branchSpec = "refs/heads/*"
    authMethod = password {
        userName = "@alexaaaanth"
        password = "credentialsJSON:49090f46-c1f8-4eb4-8c0e-fde2d892a530"
    }
})
