package autoTests.cucumber.hooks;

import driver.DriverSingleton;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class DriverHooks {

	private static final String URL = "http://localhost:8080/ui/#login";

	@Before
	public void browserSetUp() {
		System.setProperty("browser", "chrome");
		//System.setProperty("headless", "true");
		DriverSingleton.getDriver().get(URL);
	}

	@After
	public void tearDownAndCatchException(Scenario scenario) {
		if (scenario.isFailed()) {
			byte[] screenshot = ((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenshot, "image/png", "screenshot");
		}
		DriverSingleton.closeDriver();
	}
}
