package autoTests.cucumber.stepDefinition;

import autoTests.cucumber.utils.ScenarioContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pages.reportPortalPages.DashBoardPage;
import pages.reportPortalPages.ReportPortalLogInPage;

import java.util.List;

public class DashboardsSteps {

	DashBoardPage dashBoardPage = new DashBoardPage();
	ReportPortalLogInPage reportPortalLogInPage = new ReportPortalLogInPage();
	private ScenarioContext scenarioContext;

	public DashboardsSteps() {
		this.scenarioContext = new ScenarioContext();
	}

	@Given("User open Report Portal main page")
	public void userOpenRPMainPage() {
		reportPortalLogInPage.verifyLogInPageIsOpened();
	}

	@When("User log in into Report Portal")
	public void userLigInIntoRP() {
		reportPortalLogInPage.inputUserNameOnRPLogInPage();
		reportPortalLogInPage.inputPasswordOnRPLogInPage();
		reportPortalLogInPage.clickLogInButton();
	}

	@Then("User verify that he is logged in")
	public void userVerifyLoggedIn() {
		reportPortalLogInPage.verifyUserLoggedInRP();
	}

	@When("User click 'Add New Dashboard' button")
	public void userAddClickDashboardButton() {
		dashBoardPage.clickAddNewDashboardButton();
	}

	@Then("User enter dashboard name {string}")
	public void userEnterDashboardName(String dashboardName) {
		dashBoardPage.enterDashBoardName(dashboardName);
	}

	@And("User enter dashboard description {string}")
	public void userEnterDashboardDescription(String dashboardDescription) {
		dashBoardPage.enterDashboardDescription(dashboardDescription);
	}

	@Then("User click 'Add' button in modal")
	public void userClickAddButton() {
		dashBoardPage.clickAddDashboardButton();
	}

	@And("User click 'All Dashboard' title")
	public void userClickAllDashboardTitle() {
		dashBoardPage.clickAllDashboardsButton();
	}

	@Then("User verified dashboards with name {string} is created")
	public void userVerifiedDashboardsIsCreated(String dashboardName) {
		Assert.assertTrue(dashBoardPage.verifyDashboardNameIsDisplayed(dashboardName), String.format("Dashboard with name %s is displayed", dashboardName));
	}

	@Given("User add dashboards with following Names:")
	public void userAddDashboardsWithFollowingNames(DataTable table) {
		List<String> dashboardNames = table.asList();
		scenarioContext.setContext("DASHBOARD_NAME", dashboardNames);
		for (String name : dashboardNames) {
			dashBoardPage.clickAddNewDashboardButton()
					.enterDashBoardName(name)
					.clickAddDashboardButton()
					.clickAllDashboardsButton();
		}
	}

	@When("User delete created dashboards")
	public void userDeleteCreatedDashboards() {
		List<String> dashboardNames = (List<String>) scenarioContext.getContext("DASHBOARD_NAME");
		for (String name : dashboardNames) {
			dashBoardPage.deleteDashboardByName(name)
					.clickConfirmDeleteDashboardButton();
		}
	}

	@Then("User verified dashboards is deleted")
	public void userVerifiedDashboardsIsDeleted() {
		List<String> dashboardNames = (List<String>) scenarioContext.getContext("DASHBOARD_NAME");
		for (String name : dashboardNames) {
			Assert.assertTrue(dashBoardPage.verifyDashboardNameIsNotDisplayed(name), String.format("Dashboard with name %s is deleted", name));
		}
	}
}
