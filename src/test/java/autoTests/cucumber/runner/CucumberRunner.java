package autoTests.cucumber.runner;

import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "classpath:src/test/resources/cucumber/features", glue = "autoTests.cucumber")
public class CucumberRunner extends AbstractTestNGCucumberTests {

}