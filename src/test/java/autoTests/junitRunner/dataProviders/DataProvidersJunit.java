package autoTests.junitRunner.dataProviders;

import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class DataProvidersJunit {

	public static Stream<Arguments> dashboardNameDataProvider() {
		return Stream.of(
				Arguments.of("Dashboard1"),
				Arguments.of("Dashboard2"),
				Arguments.of("Dashboard3"),
				Arguments.of("Dashboard4"),
				Arguments.of("Dashboard5")
		);
	}

	public static Stream<Arguments> dashboardDescriptionDataProvider() {
		return Stream.of(
				Arguments.of("Dashboard1", "Description1"),
				Arguments.of("Dashboard2", "Description2"),
				Arguments.of("Dashboard3", "Description3"),
				Arguments.of("Dashboard4", "Description4"),
				Arguments.of("Dashboard5", "Description5")
		);
	}
}
