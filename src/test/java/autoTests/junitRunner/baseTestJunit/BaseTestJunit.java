package autoTests.junitRunner.baseTestJunit;

import driver.DriverSingleton;
import io.qameta.allure.Allure;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.ByteArrayInputStream;

public abstract class BaseTestJunit {

	private static final String URL = "http://localhost:8080/ui/#login";

	@BeforeEach
	public void browserSetUp() {
		System.setProperty("browser", "firefox");
	//	System.setProperty("headless", "true");
		DriverSingleton.getDriver().get(URL);
	}

	@AfterEach
	public void tearDownAndCatchException() {
		Allure.addAttachment("Screen on test failure", new ByteArrayInputStream(((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.BYTES)));
		DriverSingleton.closeDriver();
	}
}