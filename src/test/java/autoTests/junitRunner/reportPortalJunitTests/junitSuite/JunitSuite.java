package autoTests.junitRunner.reportPortalJunitTests.junitSuite;
import autoTests.junitRunner.reportPortalJunitTests.DashboardsJUnitTests;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({ DashboardsJUnitTests.class})
class JunitSuite {
}