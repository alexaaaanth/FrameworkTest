package autoTests.junitRunner.reportPortalJunitTests;

import autoTests.junitRunner.baseTestJunit.BaseTestJunit;
import io.qameta.allure.junit5.AllureJunit5;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import pages.reportPortalPages.DashBoardPage;
import pages.reportPortalPages.ReportPortalLogInPage;

@ExtendWith(AllureJunit5.class)
public class DashboardsJUnitTests extends BaseTestJunit {

	@Order(1)
	@ParameterizedTest
	@MethodSource("autoTests.junitRunner.dataProviders.DataProvidersJunit#dashboardNameDataProvider")
	@Execution(ExecutionMode.CONCURRENT)
	public void addDashboardsTest(String dashboardName) {
		DashBoardPage dashBoardPage = new DashBoardPage();
		ReportPortalLogInPage reportPortalLogInPage = new ReportPortalLogInPage();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.clickAddNewDashboardButton()
				.enterDashBoardName(dashboardName)
				.clickAddDashboardButton()
				.clickAllDashboardsButton();
		Assertions.assertTrue(dashBoardPage.verifyDashboardNameIsDisplayed(dashboardName), String.format("Dashboard with name %s is displayed", dashboardName));
	}

	@Order(2)
	@ParameterizedTest
	@MethodSource("autoTests.junitRunner.dataProviders.DataProvidersJunit#dashboardDescriptionDataProvider")
	@Execution(ExecutionMode.CONCURRENT)
	public void updateDescriptionDashboardsTest(String dashboardName, String dashboardDescription) {
		DashBoardPage dashBoardPage = new DashBoardPage();
		ReportPortalLogInPage reportPortalLogInPage = new ReportPortalLogInPage();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.editDashboardByName(dashboardName)
				.enterDashboardDescription(dashboardDescription)
				.clickUpdateDashBoardButton();
		Assertions.assertTrue(dashBoardPage.verifyDashboardsDescriptionIsDisplayed(dashboardDescription), String.format("Dashboard updated with description %s ", dashboardName));
	}

	@Order(3)
	@ParameterizedTest
	@MethodSource("autoTests.junitRunner.dataProviders.DataProvidersJunit#dashboardNameDataProvider")
	@Execution(ExecutionMode.CONCURRENT)
	public void deleteDashboardsTest(String dashboardName) {
		DashBoardPage dashBoardPage = new DashBoardPage();
		ReportPortalLogInPage reportPortalLogInPage = new ReportPortalLogInPage();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.deleteDashboardByName(dashboardName)
				.clickConfirmDeleteDashboardButton();
		Assertions.assertTrue(dashBoardPage.verifyDashboardNameIsNotDisplayed(dashboardName), String.format("Dashboard with name %s is deleted", dashboardName));
	}
}
