package autoTests.api;

import api.ApiClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class ApiClientTest {

	private static ApiClient apiClient;

	@BeforeAll
	public static void init() {
		apiClient = new ApiClient();
	}

	@Test
	public void positiveGetDashboardTest() {
		try (CloseableHttpResponse response = apiClient.getResource("/api/v1/default_personal/dashboard/14")) {
			Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void negativeGetDashboardTest() throws IOException {
		CloseableHttpResponse response = null;
		try {
			response = apiClient.getResource("/api/v1/default_personal/dashboard/12345626");
			Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, response.getCode());
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Test
	public void positiveTestPostCreateDashboard() throws IOException {
		String jsonString = "{ \"description\": \"apiTestDescription1\", \"name\": \"ApiDashboard1\" }";
		try (CloseableHttpResponse response = apiClient.postResource("/api/v1/default_personal/dashboard/", jsonString)) {
			Assertions.assertEquals(HttpStatus.SC_CREATED, response.getCode());
		}
	}

	@Test
	public void shouldReturnErrorPostDashboardBadJson() throws IOException {
		String badJsonString = "{ \"description\":: \"string\", \"name\":: \"string\" }";
		try (CloseableHttpResponse response = apiClient.postResource("/api/v1/default_personal/dashboard/", badJsonString)) {
			Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.getCode());
		}
	}

	@Test
	public void shouldReturnErrorPostDashboardBadUrl() throws IOException {
		String jsonString = "{ \"description\": \"string\", \"name\": \"string\" }";
		try (CloseableHttpResponse response = apiClient.postResource("/api/v1/default_personal/dashboard/incorrect_endpoint", jsonString)) {
			Assertions.assertEquals(HttpStatus.SC_METHOD_NOT_ALLOWED, response.getCode());
		}
	}

	@Test
	public void positivePutRequestAddWidget() throws IOException {
		String jsonString = "{\n"
				+ "  \"addWidget\": {\n"
				+ "    \"widgetId\": 12,\n"
				+ "    \"widgetName\": \"test\",\n"
				+ "    \"widgetOptions\": {},\n"
				+ "    \"widgetPosition\": {\n"
				+ "      \"positionX\": 15,\n"
				+ "      \"positionY\": 30\n"
				+ "    },\n"
				+ "    \"widgetSize\": {\n"
				+ "      \"height\": 10,\n"
				+ "      \"width\": 10\n"
				+ "    },\n"
				+ "    \"widgetType\": \"type\"\n"
				+ "  }\n"
				+ "}";
		try (CloseableHttpResponse response = apiClient.putResource("/api/v1/default_personal/dashboard/128/add", jsonString)) {
			Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
		}
	}

	@Test
	public void negativePutRequestNonexistentResource() throws IOException {
		String jsonString = "{\n"
				+ "  \"addWidget\": {\n"
				+ "    \"widgetId\": 12,\n"
				+ "    \"widgetName\": \"test\",\n"
				+ "    \"widgetOptions\": {},\n"
				+ "    \"widgetPosition\": {\n"
				+ "      \"positionX\": 15,\n"
				+ "      \"positionY\": 30\n"
				+ "    },\n"
				+ "    \"widgetSize\": {\n"
				+ "      \"height\": 10,\n"
				+ "      \"width\": 10\n"
				+ "    },\n"
				+ "    \"widgetType\": \"type\"\n"
				+ "  }\n"
				+ "}";
		try (CloseableHttpResponse response = apiClient.putResource("/api/v1/default_personal/dashboard/126/add", jsonString)) {
			Assertions.assertEquals(HttpStatus.SC_NOT_FOUND, response.getCode());
		}
	}

	@Test
	public void negativePutRequestInvalidData() throws IOException {
		String jsonString = "{\n"
				+ "  \"addWidget\": {\n"
				+ "    \"widgetName\": \"test\",\n"
				+ "    \"widgetPosition\": {\n"
				+ "      \"positionY\": 30\n"
				+ "    },\n"
				+ "    \"widgetSize\": {\n"
				+ "      \"height\": 10,\n"
				+ "    },\n"
				+ "    \"widgetType\": \"type\"\n"
				+ "  }\n"
				+ "}";
		try (CloseableHttpResponse response = apiClient.putResource("/api/v1/default_personal/dashboard/126/add", jsonString)) {
			Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.getCode());
		}
	}

	@Test
	public void positiveDeleteRequest() throws IOException {
		try (CloseableHttpResponse response = apiClient.deleteResource("/api/v1/default_personal/dashboard/129")) {
			Assertions.assertEquals(HttpStatus.SC_OK, response.getCode());
		}
	}

	@Test
	public void negativeDeleteRequestNonexistentResource() throws IOException {
		try (CloseableHttpResponse response = apiClient.deleteResource("/api/v1/default_personal/dashboard/nonexistent")) {
			Assertions.assertEquals(HttpStatus.SC_BAD_REQUEST, response.getCode());
		}
	}
}
