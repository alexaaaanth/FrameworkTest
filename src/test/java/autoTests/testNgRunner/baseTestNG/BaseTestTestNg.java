package autoTests.testNgRunner.baseTestNG;

import driver.DriverSingleton;
import io.qameta.allure.Allure;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.ByteArrayInputStream;

public abstract class BaseTestTestNg {
	protected Logger logger = LogManager.getRootLogger();
	private static final String URL = "http://localhost:8080/ui/#login";
	@BeforeMethod
	public void browserSetUp() {
		System.setProperty("browser", "chrome");
		//System.setProperty("headless", "true");
		DriverSingleton.getDriver().get(URL);
	}

	@AfterMethod
	public void tearDownAndCatchException(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			Allure.addAttachment("Screen on test failure", new ByteArrayInputStream(((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.BYTES)));
		}
		logger.info("WebDriver closed");
		DriverSingleton.closeDriver();
	}
}
