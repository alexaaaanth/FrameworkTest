package autoTests.testNgRunner.baseTestNG.selenide;

import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.Configuration;
import io.qameta.allure.Allure;
import notificationsForTeams.TestNotification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.ByteArrayInputStream;

public abstract class BaseTestSelenide {

	protected Logger logger = LogManager.getRootLogger();
	private static final String URL = "http://localhost:8080/ui/#login";

	@BeforeMethod
	public void browserSetUp() {
		Configuration.browser = "chrome";
		Configuration.timeout = 30000;
		open(URL);
		TestNotification.sendNotification("Test started - " + this.getClass().getName());
	}

	@AfterMethod(alwaysRun = true)
	public void tearDownAndCatchException(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			Allure.addAttachment("Screen on test failure", new ByteArrayInputStream(screenshot(OutputType.BYTES)));
			TestNotification.sendNotification("Test failed - " + this.getClass().getName());
		}
		else {
			TestNotification.sendNotification("Test passed - " + this.getClass().getName());
		}
		logger.info("WebDriver closed");
		closeWebDriver();
	}
}
