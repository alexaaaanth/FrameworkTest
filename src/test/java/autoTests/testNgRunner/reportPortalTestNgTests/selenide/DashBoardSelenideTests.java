package autoTests.testNgRunner.reportPortalTestNgTests.selenide;

import autoTests.testNgRunner.baseTestNG.selenide.BaseTestSelenide;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.selenide.DashBoardPageSelenide;
import pages.selenide.LogInPageSelenide;
import pages.selenide.WidgetPageSelenide;
@Listeners({ ReportPortalTestNGListener.class})
public class DashBoardSelenideTests extends BaseTestSelenide {

	@Test(priority = 1)
	public void addDashboardsTest() throws InterruptedException {
		DashBoardPageSelenide dashBoardPage = new DashBoardPageSelenide();
		LogInPageSelenide reportPortalLogInPage = new LogInPageSelenide();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.clickAddNewDashboardButton()
				.enterDashBoardName("Selenide Dashboard")
				.clickAddDashboardButton()
				.clickAllDashboardsButton();
		Assert.assertTrue(dashBoardPage.verifyDashboardNameIsDisplayed("Selenide Dashboard"), String.format("Dashboard with name %s is displayed", "Selenide Dashboard"));
	}

	@Test(priority = 3)
	public void deleteDashboardsTest() throws InterruptedException {
		DashBoardPageSelenide dashBoardPage = new DashBoardPageSelenide();
		LogInPageSelenide reportPortalLogInPage = new LogInPageSelenide();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.deleteDashboardByName("Selenide Dashboard")
				.clickConfirmDeleteDashboardButton();
		Assert.assertTrue(dashBoardPage.verifyDashboardNameIsNotDisplayed("Selenide Dashboard"), String.format("Dashboard with name %s is deleted", "Selenide Dashboard"));
	}

	@Test(priority = 2)
	public void updateDescriptionDashboardsTest() throws InterruptedException {
		DashBoardPageSelenide dashBoardPage = new DashBoardPageSelenide();
		LogInPageSelenide reportPortalLogInPage = new LogInPageSelenide();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.editDashboardByName("Selenide Dashboard")
				.enterDashboardDescription("Selenide Dashboard Description")
				.clickUpdateDashBoardButton();
		Assert.assertTrue(dashBoardPage.verifyDashboardsDescriptionIsDisplayed("Selenide Dashboard Description"), String.format("Dashboard updated with description %s ", "Selenide Dashboard Description"));
	}
}
