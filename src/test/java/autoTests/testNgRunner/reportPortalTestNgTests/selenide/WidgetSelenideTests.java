package autoTests.testNgRunner.reportPortalTestNgTests.selenide;

import autoTests.testNgRunner.baseTestNG.selenide.BaseTestSelenide;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.selenide.LogInPageSelenide;
import pages.selenide.WidgetPageSelenide;

@Listeners({ ReportPortalTestNGListener.class})
public class WidgetSelenideTests extends BaseTestSelenide {

	@Test
	public void addNewWidgetToDashboard() throws InterruptedException {
		WidgetPageSelenide widgetPageSelenide = new WidgetPageSelenide();
		LogInPageSelenide reportPortalLogInPage = new LogInPageSelenide();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.clickAddNewDashboardButton()
				.enterDashBoardName("Dashboard for Widget")
				.clickAddDashboardButton()
				.clickAddNewWidget()
				.chooseWidgetType("Launch statistics chart")
				.chooseDemoData()
				.addWidgetName("Widget 1");
		Assert.assertTrue(widgetPageSelenide.verifyWidgetIsAdded("Widget 1"), String.format("Widget added with name %s ", "Widget 1"));
	}

	@Test
	public void changeWidgetsOrder() throws InterruptedException {
		WidgetPageSelenide widgetPageSelenide = new WidgetPageSelenide();
		LogInPageSelenide reportPortalLogInPage = new LogInPageSelenide();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.clickAddNewDashboardButton()
				.enterDashBoardName("Dashboard for order Widgets")
				.clickAddDashboardButton()
				.clickAddNewWidget()
				.chooseWidgetType("Launch statistics chart")
				.chooseDemoData()
				.addWidgetName("Statistics Widget")
				.clickAddNewWidget()
				.chooseWidgetType("Launches duration chart")
				.chooseDemoData()
				.addWidgetName("Duration Widget");
		Assert.assertTrue(widgetPageSelenide.verifyWidgetsChangedOrder("Duration Widget", "Statistics Widget"));
	}

	@Test
	public void removeWidgetFromDashboard() throws InterruptedException {
		WidgetPageSelenide widgetPageSelenide = new WidgetPageSelenide();
		LogInPageSelenide reportPortalLogInPage = new LogInPageSelenide();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.clickAddNewDashboardButton()
				.enterDashBoardName("Dashboard for delete Widget")
				.clickAddDashboardButton()
				.clickAddNewWidget()
				.chooseWidgetType("Launch statistics chart")
				.chooseDemoData()
				.addWidgetName("Statistics Widget")
				.deleteWidget("Statistics Widget")
				.confirmDeleteWidget();
		Assert.assertTrue(widgetPageSelenide.verifyWidgetIsDeleted("Statistics Widget"), String.format("Widget deleted with name %s ", "Statistics Widget"));
	}
}
