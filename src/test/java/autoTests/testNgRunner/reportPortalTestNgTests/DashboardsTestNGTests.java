package autoTests.testNgRunner.reportPortalTestNgTests;

import autoTests.testNgRunner.baseTestNG.BaseTestTestNg;
import autoTests.testNgRunner.dataProviders.DataProvidersTestNg;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.reportPortalPages.DashBoardPage;
import pages.reportPortalPages.ReportPortalLogInPage;

public class DashboardsTestNGTests extends BaseTestTestNg {

	@Test(priority = 1, dataProvider = "DashboardsName", dataProviderClass = DataProvidersTestNg.class)
	public void addDashboardsTest(String dashboardName) {
		DashBoardPage dashBoardPage = new DashBoardPage();
		ReportPortalLogInPage reportPortalLogInPage = new ReportPortalLogInPage();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.clickAddNewDashboardButton()
				.enterDashBoardName(dashboardName)
				.clickAddDashboardButton()
				.clickAllDashboardsButton();
		Assert.assertTrue(dashBoardPage.verifyDashboardNameIsDisplayed(dashboardName), String.format("Dashboard with name %s is displayed", dashboardName));
	}

	@Test(priority = 3, dataProvider = "DashboardsName", dataProviderClass = DataProvidersTestNg.class)
	public void deleteDashboardsTest(String dashboardName) {
		DashBoardPage dashBoardPage = new DashBoardPage();
		ReportPortalLogInPage reportPortalLogInPage = new ReportPortalLogInPage();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.deleteDashboardByName(dashboardName)
				.clickConfirmDeleteDashboardButton();
		Assert.assertTrue(dashBoardPage.verifyDashboardNameIsNotDisplayed(dashboardName), String.format("Dashboard with name %s is deleted", dashboardName));
	}

	@Test(priority = 2, dataProvider = "DashboardsDescription", dataProviderClass = DataProvidersTestNg.class)
	public void updateDescriptionDashboardsTest(String dashboardName, String dashboardDescription) {
		DashBoardPage dashBoardPage = new DashBoardPage();
		ReportPortalLogInPage reportPortalLogInPage = new ReportPortalLogInPage();
		reportPortalLogInPage.verifyLogInPageIsOpened()
				.inputUserNameOnRPLogInPage()
				.inputPasswordOnRPLogInPage()
				.clickLogInButton()
				.verifyDashboardTitleIsAppears()
				.editDashboardByName(dashboardName)
				.enterDashboardDescription(dashboardDescription)
				.clickUpdateDashBoardButton();
		Assert.assertTrue(dashBoardPage.verifyDashboardsDescriptionIsDisplayed(dashboardDescription), String.format("Dashboard updated with description %s ", dashboardName));
	}
}
