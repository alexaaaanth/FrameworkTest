package autoTests.testNgRunner.dataProviders;

import org.testng.annotations.DataProvider;

public class DataProvidersTestNg {

	@DataProvider(name = "DashboardsName", parallel = true)
	public static Object[][] dashboardNameDataProvider() {
		return new Object[][] {
				{ "Dashboard1" },
				{ "Dashboard2" },
				{ "Dashboard3" },
				{ "Dashboard4" },
				{ "Dashboard5" },
		};
	}

	@DataProvider(name = "DashboardsDescription", parallel = true)
	public static Object[][] dashboardDescriptionDataProvider() {
		return new Object[][] {
				{ "Dashboard1", "Description1" },
				{ "Dashboard2", "Description2" },
				{ "Dashboard3", "Description3" },
				{ "Dashboard4", "Description4" },
				{ "Dashboard5", "Description5" },
		};
	}
}