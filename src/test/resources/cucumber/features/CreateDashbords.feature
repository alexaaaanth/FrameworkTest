Feature: User have ability to create new Dashboard

  Background:
    Given User open Report Portal main page
    When User log in into Report Portal
    Then User verify that he is logged in

  Scenario Outline: Create new Dashboards with different Names
    Given User click 'Add New Dashboard' button
    When User enter dashboard name "<Dashboard name>"
    Then User click 'Add' button in modal
    And User click 'All Dashboard' title
    Then User verified dashboards with name "<Dashboard name>" is created
    Examples:
      | Dashboard name       |
      | Test cucumber        |
      | Test cucumber second |
