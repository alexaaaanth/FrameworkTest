Feature: User have ability to delete new Dashboard

  Background:
    Given User open Report Portal main page
    When User log in into Report Portal
    Then User verify that he is logged in

  Scenario: Delete new Dashboards with different Names
    Given User add dashboards with following Names:
      | Dashboard1 |
      | Dashboard2 |
      | Dashboard3 |
    When User delete created dashboards
    Then User verified dashboards is deleted


