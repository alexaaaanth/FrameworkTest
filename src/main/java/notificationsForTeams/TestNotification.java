package notificationsForTeams;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class TestNotification {

	private static final String WEBHOOK_URL = "https://epam.webhook.office.com/webhookb2/6aa1316a-961a-4dae-8037-95b4ea664332@b41b72d0-4e9f-4c26-8a69-f949f367c91d/IncomingWebhook/3b7415e7d9c540c6b763625d154e0b70/7f4861bb-a5db-4336-9393-94f90ffeb8e4";
	private static final Logger logger = LogManager.getLogger(TestNotification.class);

	public static void sendNotification(String messageText) {
		try {
			String message = "{\n" +
					"  \"@type\": \"MessageCard\",\n" +
					"  \"title\": \"" + messageText + "\",\n" +
					"  \"text\": \" \"\n" +
					"}";
			HttpClient client = HttpClient.newBuilder()
					.version(HttpClient.Version.HTTP_2)
					.build();
			HttpRequest request = HttpRequest.newBuilder()
					.uri(URI.create(WEBHOOK_URL))
					.header("Content-Type", "application/json")
					.POST(HttpRequest.BodyPublishers.ofString(message))
					.timeout(Duration.ofSeconds(10))
					.build();
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			logger.info("Notification sent. Response code: " + response.statusCode());
		} catch (Exception e) {
			logger.error("Error while sending notification.", e);
		}
	}
}