package customException;

public class PropertyLoadException extends RuntimeException {

	public PropertyLoadException(String message, Throwable cause) {
		super(message, cause);
	}
}
