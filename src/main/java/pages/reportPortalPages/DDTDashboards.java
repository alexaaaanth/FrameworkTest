package pages.reportPortalPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.basePage.BasePage;

public class DDTDashboards extends BasePage {

	@FindBy(xpath = "//button[contains(@class,'ghostButton')]//span[text()='Add new widget'][1]")
	private WebElement addNewWidget;

	public DDTDashboards clickAddNewWidget() {
		click(addNewWidget);
		return this;
	}

}
