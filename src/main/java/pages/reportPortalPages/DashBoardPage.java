package pages.reportPortalPages;

import driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.basePage.BasePage;

public class DashBoardPage extends BasePage {

	private static final String DASHBOARD_NAMES = "//div[contains(@class,'gridRow')]//a[text()='%s']";
	private static final String DASHBOARD_DELETE = "//a[text()='%s']//parent::div//i[contains(@class,'delete')]//parent::div";
	private static final String DASHBOARD_EDIT = "//a[text()='%s']//parent::div//i[contains(@class,'icon-pencil')]//parent::div";
	private static final String DASHBOARD_DESCRIPTION = "//div[contains(@class,'dashboardTable__description')][text()='%s']";
	@FindBy(xpath = "//span[@title='All Dashboards']")
	private WebElement dashBoardTitle;
	@FindBy(xpath = "//div[contains(@class,'gridRow')]//a[text()='TestDDTDashboard']")
	private WebElement dashboardDDT;
	@FindBy(xpath = "//div[contains(@class,'addDashboardButton')]//button")
	private WebElement addNewDashboardBTN;
	@FindBy(xpath = "//input[contains(@placeholder,'Enter dashboard name')]")
	private WebElement dashboardNameField;
	@FindBy(xpath = "//button[text()='Add']")
	private WebElement addDashboardButton;
	@FindBy(xpath = "//li[contains(@class,'pageBreadcrumbs')]//a")
	private WebElement allDashboardsButton;
	@FindBy(xpath = "//button[text()='Delete']")
	private WebElement confirmDeleteDashboardButton;
	@FindBy(xpath = "//textarea[contains(@placeholder,'Enter dashboard description')]")
	private WebElement dashboardDescriptionField;
	@FindBy(xpath = "//button[text()='Update']")
	private WebElement updateDashboardButton;

	public DashBoardPage verifyDashboardTitleIsAppears() {
		isDisplayed(dashBoardTitle);
		return this;
	}

	public DDTDashboards clickDDTDashboard() {
		click(dashboardDDT);
		return new DDTDashboards();
	}

	public DashBoardPage clickAddNewDashboardButton() {
		click(addNewDashboardBTN);
		return this;
	}

	public DashBoardPage enterDashBoardName(String dashboardName) {
		sendKeys(dashboardNameField, dashboardName);
		return this;
	}

	public DashBoardPage clickAddDashboardButton() {
		click(addDashboardButton);
		return this;
	}

	public boolean verifyDashboardNameIsDisplayed(String dashboardName) {
		return isDisplayed(DriverSingleton.getDriver().findElement(By.xpath(createXpathWithOneArgument(DASHBOARD_NAMES, dashboardName))));
	}

	public boolean verifyDashboardNameIsNotDisplayed(String dashboardName) {
		return isNotDisplayed(By.xpath(createXpathWithOneArgument(DASHBOARD_NAMES, dashboardName)));
	}

	public DashBoardPage clickAllDashboardsButton() {
		click(allDashboardsButton);
		return this;
	}

	public DashBoardPage deleteDashboardByName(String dashboardName) {
		click(DriverSingleton.getDriver().findElement(By.xpath(createXpathWithOneArgument(DASHBOARD_DELETE, dashboardName))));
		return this;
	}

	public DashBoardPage clickConfirmDeleteDashboardButton() {
		click(confirmDeleteDashboardButton);
		return this;
	}

	public DashBoardPage editDashboardByName(String dashboardName) {
		click(DriverSingleton.getDriver().findElement(By.xpath(createXpathWithOneArgument(DASHBOARD_EDIT, dashboardName))));
		return this;
	}

	public DashBoardPage enterDashboardDescription(String dashboardDescription) {
		sendKeys(dashboardDescriptionField, dashboardDescription);
		return this;
	}

	public DashBoardPage clickUpdateDashBoardButton() {
		updateDashboardButton.click();
		return this;
	}

	public boolean verifyDashboardsDescriptionIsDisplayed(String dashboardName) {
		return isDisplayed(DriverSingleton.getDriver().findElement(By.xpath(createXpathWithOneArgument(DASHBOARD_DESCRIPTION, dashboardName))));
	}
}
