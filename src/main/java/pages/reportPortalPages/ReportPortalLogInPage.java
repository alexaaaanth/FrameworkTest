package pages.reportPortalPages;

import driver.DriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.basePage.BasePage;

public class ReportPortalLogInPage extends BasePage {

	private static final String SIDE_BAR_BUTTON = "//span[contains(@class,'sidebarButton')][contains(text(),'%s')]";
	@FindBy(xpath = "//div[contains(@class,'externalLoginBlock')]//button")
	private WebElement loginWithEpamBTN;
	@FindBy(xpath = "//div[contains(@class,'userBlock__avatar-wrapper')]")
	private WebElement avatarPicture;
	@FindBy(xpath = "//div[contains(@class,'pageBlockContainer')]")
	private WebElement logInPageContainer;
	@FindBy(xpath = "//input[@type='email']")
	private WebElement inputLogInField;
	@FindBy(xpath = "//input[@type='password']")
	private WebElement inputPasswordField;
	@FindBy(xpath = "//input[@type='submit']")
	private WebElement nextButton;
	@FindBy(xpath = "//div[@class='display-sign-container']")
	private WebElement signInRequestModal;
	@FindBy(xpath = "//input[@name='login']")
	private WebElement logInFieldRP;
	@FindBy(xpath = "//input[@name='password']")
	private WebElement passwordFieldRP;
	@FindBy(xpath = "//button[@type='submit']")
	private WebElement logInButton;


	public ReportPortalLogInPage clickLogInWithEpamButton() {
		loginWithEpamBTN.click();
		return this;
	}

	public boolean verifyUserLoggedInRP() {
		return isDisplayed(avatarPicture);
	}

	public void clickSideBarButton(String nameOfSideBarButton) {
		click(DriverSingleton.getDriver().findElement(By.xpath(createXpathWithOneArgument(SIDE_BAR_BUTTON, nameOfSideBarButton))));
	}

	public ReportPortalLogInPage verifyLogInPageIsOpened() {
		isDisplayed(logInPageContainer);
		logger.info("Report Portal log in page is open");
		return this;
	}

	public ReportPortalLogInPage writeLogIn() {
		sendKeys(inputLogInField, getUserName());
		return this;
	}

	public ReportPortalLogInPage writePassword() {
		sendKeys(inputPasswordField, getPassword());
		return this;
	}

	public ReportPortalLogInPage clickNextBtn() {
		click(nextButton);
		return this;
	}

	public ReportPortalLogInPage signInRequestModalAppears() {
		waitElement(signInRequestModal);
		return this;
	}

	public ReportPortalLogInPage inputUserNameOnRPLogInPage() {
		sendKeys(logInFieldRP, getUserName());
		return this;
	}

	public ReportPortalLogInPage inputPasswordOnRPLogInPage() {
		sendKeys(passwordFieldRP, getPassword());
		return this;
	}

	public DashBoardPage clickLogInButton() {
		click(logInButton);
		return new DashBoardPage();
	}
}
