package pages.selenide;

import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class WidgetPageSelenide extends BasePageSelenide {

	private static final String WIDGET_TYPE = "//div[contains(@class,'widgetTypeItem')][contains(text(),'%s')]";
	private static final String WIDGET_BLOCK = "//div[contains(@class,'widgetHeader__widget-name-block')][contains(text(),'%s')]";
	private static final String WIDGET_LOCATOR = "//div[contains(@class,'widgetHeader__widget-name-block')]";
	private SelenideElement addNewWidgetBTN = $x("//div[@class='widgets-grid']//span[contains(text(),'Add new widget')]");
	private SelenideElement nextStepBTN = $x("//button//span[contains(text(),'Next step')]");
	private SelenideElement clickDataForWidget = $x("//span[contains(@class,'inputRadio__toggler')]");
	private SelenideElement inputWidgetNameField = $x("//input[@placeholder='Enter widget name']");
	private SelenideElement addWidgetBTN = $x("//button[contains(text(),'Add')]");
	private SelenideElement allDashboardsButton = $x("//li[contains(@class,'pageBreadcrumbs')]//a");
	private SelenideElement deleteWidgetBTN = $x("//div[contains(@class,'widgetHeader__control--SQilp widgetHeader__mobile-hide')][2]");
	private SelenideElement confirmationDeleteWidgetBTN = $x("//button[contains(text(),'Delete')]");

	public WidgetPageSelenide clickAddNewWidget() {
		addNewWidgetBTN.click();
		return this;
	}

	public WidgetPageSelenide chooseWidgetType(String widgetType) {
		clickWithJS($x(String.format(WIDGET_TYPE, widgetType)));
		nextStepBTN.click();
		return this;
	}

	public WidgetPageSelenide chooseDemoData() {
		scrollToElement(clickDataForWidget);
		clickDataForWidget.click();
		nextStepBTN.click();
		return this;
	}

	public WidgetPageSelenide addWidgetName(String widgetName) {
		inputWidgetNameField.clear();
		inputWidgetNameField.sendKeys(widgetName);
		addWidgetBTN.click();
		return this;
	}

	public boolean verifyWidgetIsAdded(String widgetName) {
		waitUntilElementVisible($x(String.format(WIDGET_BLOCK, widgetName)));
		return $x(String.format(WIDGET_BLOCK, widgetName)).isDisplayed();
	}

	public DashBoardPageSelenide clickAllDashboardsButton() {
		allDashboardsButton.click();
		return new DashBoardPageSelenide();
	}

	public boolean verifyWidgetsChangedOrder(String sourceElement, String targetElement) {
		List<String> before = getWidgetOrder();
		dragAndDrop(String.format(WIDGET_BLOCK, sourceElement), String.format(WIDGET_BLOCK, targetElement));
		List<String> after = getWidgetOrder();
		return !before.equals(after);
	}

	public List<String> getWidgetOrder() {
		return $$x(WIDGET_LOCATOR).texts();
	}

	public WidgetPageSelenide deleteWidget(String widgetName) {
		waitUntilElementVisible($x(String.format(WIDGET_BLOCK, widgetName)));
		hoverOverElement($x(String.format(WIDGET_BLOCK, widgetName)));
		deleteWidgetBTN.click();
		return this;
	}

	public WidgetPageSelenide confirmDeleteWidget() {
		waitUntilElementVisible(confirmationDeleteWidgetBTN);
		confirmationDeleteWidgetBTN.click();
		return this;
	}

	public boolean verifyWidgetIsDeleted(String widgetName) {
		waitUntilElementDisappear($x(String.format(WIDGET_BLOCK, widgetName)));
		return !$x(String.format(WIDGET_BLOCK, widgetName)).isDisplayed();
	}
}
