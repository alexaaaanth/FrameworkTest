package pages.selenide;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import com.codeborne.selenide.SelenideElement;
import pages.basePage.BasePage;
import org.openqa.selenium.By;

public class LogInPageSelenide extends BasePageSelenide {

	private static final String SIDE_BAR_BUTTON = "//span[contains(@class,'sidebarButton')][contains(text(),'%s')]";
	private SelenideElement loginWithEpamBTN = $x("//div[contains(@class,'externalLoginBlock')]//button");
	private SelenideElement avatarPicture = $x("//div[contains(@class,'userBlock__avatar-wrapper')]");
	private SelenideElement logInPageContainer = $x("//div[contains(@class,'pageBlockContainer')]");
	private SelenideElement inputLogInField = $x("//input[@type='email']");
	private SelenideElement inputPasswordField = $x("//input[@type='password']");
	private SelenideElement nextButton = $x("//input[@type='submit']");
	private SelenideElement signInRequestModal = $x("//div[@class='display-sign-container']");
	private SelenideElement logInFieldRP = $x("//input[@name='login']");
	private SelenideElement passwordFieldRP = $x("//input[@name='password']");
	private SelenideElement logInButton = $x("//button[@type='submit']");

	public LogInPageSelenide clickLogInWithEpamButton() {
		loginWithEpamBTN.click();
		return this;
	}

	public boolean verifyUserLoggedInRP() {
		return avatarPicture.shouldBe(visible).exists();
	}

	public void clickSideBarButton(String nameOfSideBarButton) {
		$(By.xpath(String.format(SIDE_BAR_BUTTON, nameOfSideBarButton))).click();
	}

	public LogInPageSelenide verifyLogInPageIsOpened() throws InterruptedException {
		Thread.sleep(30000);
		logInPageContainer.shouldBe(visible);
		logger.info("Report Portal log in page is open");
		return this;
	}

	public LogInPageSelenide writeLogIn() {
		inputLogInField.setValue(getUserName());
		return this;
	}

	public LogInPageSelenide writePassword() {
		inputPasswordField.setValue(getPassword());
		return this;
	}

	public LogInPageSelenide clickNextBtn() {
		nextButton.click();
		return this;
	}

	public LogInPageSelenide signInRequestModalAppears() {
		signInRequestModal.shouldBe(visible);
		return this;
	}

	public LogInPageSelenide inputUserNameOnRPLogInPage() {
		logInFieldRP.setValue(getUserName());
		return this;
	}

	public LogInPageSelenide inputPasswordOnRPLogInPage() {
		passwordFieldRP.setValue(getPassword());
		return this;
	}

	public DashBoardPageSelenide clickLogInButton() {
		logInButton.click();
		return new DashBoardPageSelenide();
	}
}