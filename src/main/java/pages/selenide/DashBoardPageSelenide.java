package pages.selenide;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class DashBoardPageSelenide extends BasePageSelenide {

	private static final String DASHBOARD_NAMES = "//div[contains(@class,'gridRow')]//a[text()='%s']";
	private static final String DASHBOARD_DELETE = "//a[text()='%s']//parent::div//i[contains(@class,'delete')]//parent::div";
	private static final String DASHBOARD_EDIT = "//a[text()='%s']//parent::div//i[contains(@class,'icon-pencil')]//parent::div";
	private static final String DASHBOARD_DESCRIPTION = "//div[contains(@class,'dashboardTable__description')][text()='%s']";
	private SelenideElement dashBoardTitle = $x("//span[@title='All Dashboards']");
	private SelenideElement dashboardDDT = $x("//div[contains(@class,'gridRow')]//a[text()='TestDDTDashboard']");
	private SelenideElement addNewDashboardBTN = $x("//div[contains(@class,'addDashboardButton')]//button");
	private SelenideElement dashboardNameField = $x("//input[contains(@placeholder,'Enter dashboard name')]");
	private SelenideElement addDashboardButton = $x("//button[text()='Add']");
	private SelenideElement confirmDeleteDashboardButton = $x("//button[text()='Delete']");
	private SelenideElement dashboardDescriptionField = $x("//textarea[contains(@placeholder,'Enter dashboard description')]");
	private SelenideElement updateDashboardButton = $x("//button[text()='Update']");

	public DashBoardPageSelenide verifyDashboardTitleIsAppears() {
		dashBoardTitle.isDisplayed();
		return this;
	}

	public DashBoardPageSelenide clickDDTDashboard() {
		dashboardDDT.click();
		return this;
	}

	public DashBoardPageSelenide clickAddNewDashboardButton() {
		addNewDashboardBTN.click();
		return this;
	}

	public DashBoardPageSelenide enterDashBoardName(String dashboardName) {
		dashboardNameField.setValue(dashboardName);
		return this;
	}

	public WidgetPageSelenide clickAddDashboardButton() {
		addDashboardButton.click();
		return new WidgetPageSelenide();
	}

	public boolean verifyDashboardNameIsDisplayed(String dashboardName) {
		waitUntilElementVisible($x(String.format(DASHBOARD_NAMES, dashboardName)));
		return $x(String.format(DASHBOARD_NAMES, dashboardName)).isDisplayed();
	}

	public boolean verifyDashboardNameIsNotDisplayed(String dashboardName) {
		waitUntilElementDisappear($x(String.format(DASHBOARD_NAMES, dashboardName)));
		return !$x(String.format(DASHBOARD_NAMES, dashboardName)).isDisplayed();
	}

	public DashBoardPageSelenide deleteDashboardByName(String dashboardName) {
		$x(String.format(DASHBOARD_DELETE, dashboardName)).click();
		return this;
	}

	public DashBoardPageSelenide clickConfirmDeleteDashboardButton() {
		confirmDeleteDashboardButton.click();
		return this;
	}

	public DashBoardPageSelenide editDashboardByName(String dashboardName) {
		$x(String.format(DASHBOARD_EDIT, dashboardName)).click();
		return this;
	}

	public DashBoardPageSelenide enterDashboardDescription(String dashboardDescription) {
		dashboardDescriptionField.setValue(dashboardDescription);
		return this;
	}

	public DashBoardPageSelenide clickUpdateDashBoardButton() {
		updateDashboardButton.click();
		return this;
	}

	public boolean verifyDashboardsDescriptionIsDisplayed(String dashboardName) {
		waitUntilElementVisible($x(String.format(DASHBOARD_DESCRIPTION, dashboardName)));
		return $x(String.format(DASHBOARD_DESCRIPTION, dashboardName)).isDisplayed();
	}
}