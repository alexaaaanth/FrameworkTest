package pages.selenide;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

public abstract class BasePageSelenide {

	protected Logger logger = LogManager.getRootLogger();

	public BasePageSelenide() {
	}

	private JavascriptExecutor getJsExecutor() {
		return (JavascriptExecutor) getWebDriver();
	}

	public boolean isDisplayed(SelenideElement element) {
		return element.isDisplayed();
	}

	public boolean isNotDisplayed(SelenideElement element) {
		return element.shouldNotBe(visible).exists();
	}

	public Properties getInfoFromFile() {
		Properties prop = new Properties();
		try (FileInputStream file = new FileInputStream("src/test/resources/test.properties")) {
			prop.load(file);
		} catch (IOException e) {
			throw new RuntimeException("Failed to load properties file", e);
		}
		return prop;
	}

	public String getUserName() {
		return getInfoFromFile().getProperty("username");
	}

	public String getPassword() {
		return getInfoFromFile().getProperty("password");
	}

	public void highlightElement(SelenideElement element) {
		executeJavaScript("arguments[0].setAttribute('style', 'border: 2px solid red;')", element);
	}

	public void dragAndDrop(String sourceElementXpath, String targetElementXpath) {
		actions().dragAndDrop($x(sourceElementXpath), $x(targetElementXpath)).perform();
		logger.info("Element has been moved");
	}

	public void scrollToElement(SelenideElement element) {
		getJsExecutor().executeScript("arguments[0].scrollIntoView();", element);
		logger.info("Scroll to element by JSExecutor: " + element);
	}

	public boolean isElementInView(SelenideElement element) {
		logger.info("Visibility of element");
		return (boolean) getJsExecutor().executeScript("return arguments[0].scrollIntoView();", element);
	}

	public void clickWithJS(SelenideElement element) {
		getJsExecutor().executeScript("arguments[0].click();", element);
		logger.info("Click element by JSExecutor");
	}

	public void waitUntilElementVisible(SelenideElement element) {
		element.shouldBe(visible, Duration.ofSeconds(5));
		logger.info("Wait visibility of element");
	}

	public void waitUntilElementDisappear(SelenideElement element) {
		element.should(disappear, Duration.ofSeconds(5));
		logger.info("Wait until element disappears");
	}

	public void hoverOverElement(SelenideElement element) {
		actions().moveToElement(element).perform();
		logger.info("Cursor hover element" + element);
	}
}