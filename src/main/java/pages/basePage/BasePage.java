package pages.basePage;

import customException.PropertyLoadException;
import driver.DriverSingleton;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Properties;

public abstract class BasePage {

	protected Logger logger = LogManager.getRootLogger();
	protected String switchToNewTab;
	protected JavascriptExecutor js;
	protected Actions action;

	WebDriverWait wait = new WebDriverWait(DriverSingleton.getDriver(), Duration.ofSeconds(60));

	public BasePage() {
		this.js = (JavascriptExecutor) DriverSingleton.getDriver();
		this.action = new Actions(DriverSingleton.getDriver());
		PageFactory.initElements(DriverSingleton.getDriver(), this);
	}

	public BasePage(String winHandleBefore) {
		this();
		this.switchToNewTab = winHandleBefore;
	}

	public WebElement findElement(By by) {
		return DriverSingleton.getDriver().findElement(by);
	}

	public List<WebElement> findElements(By by) {
		return DriverSingleton.getDriver().findElements(by);
	}

	public void waitElement(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void scrollTO(WebElement element) {
		js.executeScript("arguments[0].scrollIntoView();", element);
	}

	public void highlightElement(WebElement element) {
		js.executeScript("arguments[0]. setAttribute('style', 'border:2px solid red; background:yellow')", element);
	}

	public void doubleClick(WebElement element) {
		action.moveToElement(element).doubleClick().build().perform();
	}

	public void pause(WebElement element) {
		action.moveToElement(element).pause(java.time.Duration.ofSeconds(10)).build().perform();
	}

	public void click(WebElement element) {
		waitElement(element);
		element.click();
	}

	public void sendKeys(WebElement element, CharSequence charSequence) {
		waitElement(element);
		element.sendKeys(charSequence);
	}

	public boolean isDisplayed(WebElement element) {
		waitElement(element);
		return element.isDisplayed();
	}

	public boolean isNotDisplayed(By locator) {
		try {
			return wait.until(ExpectedConditions.invisibilityOf(DriverSingleton.getDriver().findElement(locator)));
		} catch (NoSuchElementException ex) {
			return true;
		}
	}

	public String createXpathWithOneArgument(String originalXpath, String value) {
		logger.info("Xpath with one Argument created: " + String.format(originalXpath, value));
		return String.format(originalXpath, value);
	}

	public Properties getInfoFromFile() {
		Properties prop = new Properties();
		try (FileInputStream file = new FileInputStream("src/test/resources/test.properties")) {
			prop.load(file);
		} catch (IOException e) {
			throw new PropertyLoadException("Failed to load properties file", e);
		}
		return prop;
	}

	public String getUserName() {
		Properties prop = getInfoFromFile();
		return prop.getProperty("username");
	}

	public String getPassword() {
		Properties prop = getInfoFromFile();
		return prop.getProperty("password");
	}
}
