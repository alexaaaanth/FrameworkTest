package api;

import customException.PropertyLoadException;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.Method;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.apache.hc.core5.http.message.BasicHeader;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class ApiClient {

	private static final String BASE_URL = "http://localhost:8080";
	private Properties prop;

	public ApiClient() {
		prop = new Properties();
		try (FileInputStream file = new FileInputStream("src/test/resources/test.properties")) {
			prop.load(file);
		} catch (IOException e) {
			throw new PropertyLoadException("Failed to load properties file", e);
		}
	}

	private CloseableHttpClient createAuthorizedClient() {
		return HttpClients.createDefault();
	}

	public CloseableHttpResponse sendRequest(Method method, String resource, StringEntity entity) {
		try (CloseableHttpClient client = createAuthorizedClient()) {
			ClassicRequestBuilder requestBuilder = ClassicRequestBuilder.create(method.name())
					.setUri(BASE_URL + resource)
					.addHeader(new BasicHeader(HttpHeaders.AUTHORIZATION, "Bearer " + prop.getProperty("token")));
			if ((method == Method.POST || method == Method.PUT || method == Method.DELETE) && entity != null) {
				requestBuilder.setEntity(entity);
			}
			ClassicHttpRequest request = requestBuilder.build();
			CloseableHttpResponse response = client.execute(request);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public CloseableHttpResponse getResource(String resource) {
		return sendRequest(Method.GET, resource, null);
	}

	public CloseableHttpResponse postResource(String resource, String jsonBody) {
		StringEntity stringEntity = new StringEntity(jsonBody, ContentType.APPLICATION_JSON);
		return sendRequest(Method.POST, resource, stringEntity);
	}

	public CloseableHttpResponse putResource(String resource, String jsonBody) {
		StringEntity stringEntity = new StringEntity(jsonBody, ContentType.create("application/json", StandardCharsets.UTF_8));
		return sendRequest(Method.PUT, resource, stringEntity);
	}

	public CloseableHttpResponse deleteResource(String resource) {
		return sendRequest(Method.DELETE, resource, null);
	}
}