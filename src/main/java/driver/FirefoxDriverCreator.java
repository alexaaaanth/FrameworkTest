package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class FirefoxDriverCreator implements WebDriverCreator {

	@Override
	public WebDriver createWebDriver() {
		FirefoxOptions options = new FirefoxOptions();
		if ("true".equals(System.getProperty("headless"))) {
			options.addArguments("--headless");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
		}
		WebDriverManager.firefoxdriver().setup();
		WebDriver webDriver = new FirefoxDriver(options);
		webDriver.manage().window().maximize();
		return webDriver;
	}

}
