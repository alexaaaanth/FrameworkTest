package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class SafariDriverCreator implements WebDriverCreator {

	@Override
	public WebDriver createWebDriver() {
		WebDriver webDriver = new SafariDriver();
		webDriver.manage().window().maximize();
		return webDriver;
	}
}
