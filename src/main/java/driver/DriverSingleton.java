package driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class DriverSingleton {

	private static final Logger logger = LogManager.getRootLogger();
	private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

	private DriverSingleton() {
	}

	public static WebDriver getDriver() {
		if (webDriver.get() == null) {
			if ("safari".equals(System.getProperty("browser"))) {
				SafariDriverCreator safariDriverCreator = new SafariDriverCreator();
				webDriver.set(safariDriverCreator.createWebDriver());
			} else if ("firefox".equals(System.getProperty("browser"))) {
				FirefoxDriverCreator firefoxDriverCreator = new FirefoxDriverCreator();
				webDriver.set(firefoxDriverCreator.createWebDriver());
			} else {
				ChromeDriverCreator chromeDriverCreator = new ChromeDriverCreator();
				webDriver.set(chromeDriverCreator.createWebDriver());
			}
			if (webDriver.get() == null) {
				throw new RuntimeException("Error initializing WebDriver. Check your browser system property and WebDriver creators.");
			}
			logger.info("WebDriver for thread id {} was initialized", Thread.currentThread().getId());
		}
		return webDriver.get();
	}

	public static void closeDriver() {
		if (webDriver.get() != null) {
			webDriver.get().quit();
			webDriver.remove();
			logger.info("WebDriver for thread id {} was closed", Thread.currentThread().getId());
		}
	}
}