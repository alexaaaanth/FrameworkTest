package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeDriverCreator implements WebDriverCreator {

	@Override
	public WebDriver createWebDriver() {
		ChromeOptions options = new ChromeOptions();
		if ("true".equals(System.getProperty("headless"))) {
			options.addArguments("--headless");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
		}
		WebDriverManager.chromedriver().setup();
		WebDriver webDriver = new ChromeDriver(options);
		webDriver.manage().window().maximize();
		return webDriver;
	}
}
